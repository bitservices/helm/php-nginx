################################################################################

{{- $cronFirst := (keys .Values.cron.jobs) | first }}
{{- range $key, $job := .Values.cron.jobs }}

{{- if ne $key $cronFirst }}
---
{{- end }}

apiVersion: batch/v1
kind: CronJob
metadata:
  name: "{{ $.Release.Name }}-cron-{{ $key }}"
  labels:
    k8s.bitservices.io/cron: "{{ $key }}"
{{- include "php-nginx.cron.labels" $ | indent 4 }}
spec:
  schedule: "{{ $job.schedule | default $.Values.cron.defaults.schedule }}"
  jobTemplate: 
    spec:
      template:
        metadata:
          annotations:
            k8s.bitservices.io/checksum-configmap: "{{ include (print $.Template.BasePath "/configmap.yaml") $ | sha256sum }}"
            k8s.bitservices.io/checksum-configmap-php-overrides: "{{ include (print $.Template.BasePath "/cron/configmap-php-overrides.yaml") $ | sha256sum }}"
          labels:
            k8s.bitservices.io/cron: "{{ $key }}"
{{- include "php-nginx.cron.labels" $ | indent 12 }}
        spec:
          containers:
            - name: "{{ $.Values.global.php.image.container }}"
              image: "{{ tpl $.Values.global.php.image.name $ }}:{{ tpl $.Values.global.php.image.release $ }}"
{{- if $.Values.global.php.image.forcePull }}
              imagePullPolicy: Always
{{- end }}
{{- if (($job.php).args) }}
              args:
{{- range $job.php.args }}
                - "{{ . }}"
{{- end }}
{{- end }}
{{- if (($job.php).command) }}
              command:
{{- range $job.php.command }}
                - "{{ . }}"
{{- end }}
{{- end }}
{{- if or $.Values.global.php.envSecrets $.Values.cron.php.envSecrets
          $.Values.global.php.envValues $.Values.cron.php.envValues
          (($job.php).envSecrets) (($job.php).envValues) }}
              env:
{{- if or $.Values.global.php.envSecrets
          $.Values.cron.php.envSecrets
          (($job.php).envSecrets) }}
{{- range $name, $value := merge (($job.php).envSecrets) $.Values.cron.php.envSecrets $.Values.global.php.envSecrets }}
                - name: "{{ $name }}"
                  valueFrom:
                    secretKeyRef:
                      name: "{{ $value.secretName }}"
                      key: "{{ $value.secretKey }}"
{{- end }}
{{- end }}
{{- if or $.Values.global.php.envValues
          $.Values.cron.php.envValues
          (($job.php).envValues) }}
{{- range $name, $value := merge (($job.php).envValues) $.Values.cron.php.envValues $.Values.global.php.envValues }}
                - name: "{{ $name }}"
                  value: "{{ tpl (toString $value) $ }}"
{{- end }}
{{- end }}
{{- end }}
              resources:
{{- if (($job.php).resources) }}
{{ toYaml (merge (deepCopy $job.php.resources)
                 (deepCopy $.Values.cron.defaults.php.resources)) | indent 16 }}
{{- else }}
{{ toYaml $.Values.cron.defaults.php.resources | indent 16 }}
{{- end }}
              volumeMounts:
                - name: php-overrides
                  subPath: php_overrides.ini
                  readOnly: true
                  mountPath: /usr/local/etc/php/conf.d/zz_overrides.ini
{{- if (or (eq $.Values.global.filesystem.type "efs")
           (eq $.Values.global.filesystem.type "local")) }}
                - name: shared-data
                  mountPath: "{{ $.Values.global.filesystem.mountPath }}"
{{- end }}
{{- if eq $.Values.global.framework.name "drupal" }}
                - name: secrets
                  readOnly: true
                  mountPath: /secrets
                - name: drupal-config
                  readOnly: true
                  mountPath: /var/www/web/sites/default/settings.php
                  subPath: settings.php
{{- end }}
{{- if $.Values.global.php.volumeMounts }}
{{ toYaml $.Values.global.php.volumeMounts | indent 16 }}
{{- end }}
{{- if $.Values.cron.php.volumeMounts }}
{{ toYaml $.Values.cron.php.volumeMounts | indent 16 }}
{{- end }}
{{- if ($job.php).volumeMounts }}
{{ toYaml ($job.php).volumeMounts | indent 16 }}
{{- end }}
          securityContext:
            runAsUser: 33
            runAsNonRoot: true
          serviceAccountName: "{{ $.Release.Name }}-cron-{{ $key }}"
          volumes:
            - name: php-overrides
              configMap:
                name: "{{ $.Release.Name }}-cron-{{ $key }}-php-overrides"
                defaultMode: 0440
{{- if (or (eq $.Values.global.filesystem.type "efs")
           (eq $.Values.global.filesystem.type "local")) }}
            - name: shared-data
{{- if eq $.Values.global.filesystem.type "efs" }}
              persistentVolumeClaim:
                claimName: "{{ .Release.Name }}"
{{- else if eq $.Values.global.filesystem.type "local" }}
              hostPath:
                path: /shared-data
{{- end }}
{{- end }}
{{- if eq $.Values.global.framework.name "drupal" }}
            - name: secrets
              secret:
                secretName: "{{ $.Release.Name }}"
                defaultMode: 0444
                optional: false
            - name: drupal-config
              configMap:
                name: "{{ $.Release.Name }}"
                defaultMode: 0444
{{- end }}
{{- if $.Values.global.volumes }}
{{ toYaml $.Values.global.volumes | indent 12 }}
{{- end }}
{{- if $.Values.cron.volumes }}
{{ toYaml $.Values.cron.volumes | indent 12 }}
{{- end }}
{{- if $job.volumes }}
{{ toYaml $job.volumes | indent 12 }}
{{- end }}

{{- end }}

################################################################################
