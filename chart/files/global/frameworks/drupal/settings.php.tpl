<?php
//----------------------------------------------------------------------------//
// Drupal Configuration File
// Template created by Helm (php-nginx)
//----------------------------------------------------------------------------//
// Pull in any secrets
//----------------------------------------------------------------------------//

if(file_exists("/secrets/settings.php")) {
    include("/secrets/settings.php");
}

//----------------------------------------------------------------------------//
// DB Config
//----------------------------------------------------------------------------//

$databases['default']['default']['driver'] = 'mysql';
$databases['default']['default']['prefix'] = '';

//----------------------------------------------------------------------------//
// S3FS Config
//----------------------------------------------------------------------------//

$config['s3fs.settings']['use_cname'] = TRUE;
$config['s3fs.settings']['use_https'] = TRUE;
$config['s3fs.settings']['no_rewrite_cssjs'] = TRUE;

//----------------------------------------------------------------------------//

$settings['s3fs.upload_as_private'] = TRUE;
$settings['s3fs.use_s3_for_public'] = TRUE;
$settings['s3fs.use_s3_for_private'] = TRUE;

//----------------------------------------------------------------------------//
// Folder Config
//----------------------------------------------------------------------------//

$settings['config_sync_directory'] = '/var/www/sync';
$settings['file_public_path'] = '{{ .Values.global.framework.options.drupal.publicPath }}';
$settings['file_temp_path'] = '/var/www/temp';
$settings['php_storage']['twig']['directory'] = '/var/www/twig';

//----------------------------------------------------------------------------//

$settings['file_scan_ignore_directories'] = [
  'bower_components',
  'node_modules'
];

//----------------------------------------------------------------------------//
// Reverse Proxy Config
//----------------------------------------------------------------------------//

$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = array($_SERVER['REMOTE_ADDR']);
$settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_TRUSTED_HEADER;

//----------------------------------------------------------------------------//
// Trusted Host Config
//----------------------------------------------------------------------------//

$settings['trusted_host_patterns'] = [
{{- range $trustedHostRegex := .Values.global.framework.options.drupal.trustedHostRegexs }}
  '{{ tpl $trustedHostRegex $ }}',
{{- end }}
];

//----------------------------------------------------------------------------//
// Other Config
//----------------------------------------------------------------------------//

$config['system.site']['name'] = '{{ tpl .Values.global.framework.options.drupal.name . }}';

//----------------------------------------------------------------------------//

$settings['allow_authorize_operations'] = FALSE;
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['entity_update_backup'] = TRUE;
$settings['entity_update_batch_size'] = 50;
$settings['update_free_access'] = FALSE;

//----------------------------------------------------------------------------//
