;------------------------------------------------------------------------------;

expose_php = Off

;------------------------------------------------------------------------------;

max_execution_time = {{ max .Values.web.php.config.core.maxExecutionTimeSeconds 1 }}

;------------------------------------------------------------------------------;

max_input_time = {{ max .Values.web.php.config.core.maxInputTimeSeconds 1 }}
max_input_vars = {{ .Values.web.php.config.core.maxInputVars }}

;------------------------------------------------------------------------------;

post_max_size = {{ max .Values.web.php.config.core.postMaxSizeMb 2 }}M

;------------------------------------------------------------------------------;

{{- if (gt .Values.web.php.config.core.uploadMaxFilesizeMb (sub (max .Values.web.php.config.core.postMaxSizeMb 2) 1 | float64)) }}
upload_max_filesize = {{ sub (max .Values.web.php.config.core.postMaxSizeMb 2) 1 }}M
{{- else }}
upload_max_filesize = {{ max .Values.web.php.config.core.uploadMaxFilesizeMb 1 }}M
{{- end }}

;------------------------------------------------------------------------------;
