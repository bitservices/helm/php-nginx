;------------------------------------------------------------------------------;

[www]

;------------------------------------------------------------------------------;

listen = 127.0.0.1:9000

;------------------------------------------------------------------------------;

php_admin_value[memory_limit] = {{ .Values.web.php.config.fpm.scriptMemoryLimitMb }}M

;------------------------------------------------------------------------------;

pm                   = {{ .Values.web.php.config.fpm.pm                }}
pm.start_servers     = {{ .Values.web.php.config.fpm.pmStartServers    }}
pm.max_requests      = {{ .Values.web.php.config.fpm.pmMaxRequests     }}
pm.max_children      = {{ .Values.web.php.config.fpm.pmMaxChildren     }}
pm.min_spare_servers = {{ .Values.web.php.config.fpm.pmMinSpareServers }}
pm.max_spare_servers = {{ .Values.web.php.config.fpm.pmMaxSpareServers }}

;------------------------------------------------------------------------------;

request_terminate_timeout = {{ max .Values.web.php.config.fpm.requestTerminateTimeoutSeconds (add (max .Values.web.php.config.core.maxExecutionTimeSeconds 1) (max .Values.web.php.config.core.maxInputTimeSeconds 1) 10) }}s

;------------------------------------------------------------------------------;

clear_env = {{ .Values.web.php.config.fpm.clearEnv }}

;------------------------------------------------------------------------------;

{{- range .Values.web.php.envSystem }}
env[{{ . }}] = ${{ . }}
{{- end }}

;------------------------------------------------------------------------------;

{{- range $name, $value := merge .Values.web.php.envSecrets .Values.global.php.envSecrets }}
env[{{ $name }}] = ${{ $name }}
{{- end }}

;------------------------------------------------------------------------------;

{{- range $name, $value := merge .Values.web.php.envValues .Values.global.php.envValues }}
env[{{ $name }}] = ${{ $name }}
{{- end }}

;------------------------------------------------------------------------------;
