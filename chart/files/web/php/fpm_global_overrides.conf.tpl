;------------------------------------------------------------------------------;

[global]

;------------------------------------------------------------------------------;

process_control_timeout = {{ max .Values.web.php.config.fpm.requestTerminateTimeoutSeconds (add (max .Values.web.php.config.core.maxExecutionTimeSeconds 1) (max .Values.web.php.config.core.maxInputTimeSeconds 1) 10) }}s

;------------------------------------------------------------------------------;
