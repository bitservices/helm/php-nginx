################################################################################

pid              /var/run/nginx.pid;
user             nginx;
worker_processes {{ .Values.web.nginx.workers }};

################################################################################

error_log  /var/log/nginx/error.log warn;

################################################################################

events {
    worker_connections  1024;
}

################################################################################

include /etc/nginx/conf.d/*.main.conf;

################################################################################

http {

    ############################################################################

    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    ############################################################################

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    ############################################################################

    sendfile                  on;
    tcp_nopush                on;
    tcp_nodelay               on;
    keepalive_timeout         120;
    client_max_body_size      {{ .Values.web.php.config.core.postMaxSizeMb }}M;
    client_body_buffer_size   64k;
    reset_timedout_connection on;

    ############################################################################

    include /etc/nginx/conf.d/*.http.conf;
    include /etc/nginx/conf.d/*.server.conf;

    ############################################################################

}

################################################################################

