################################################################################

server {

    listen 80 default_server;
    server_name _;

    ############################################################################

{{- if eq .Values.global.framework.name "drupal" }}
    root /var/www/web;
{{- else if eq .Values.global.framework.name "laravel" }}
    root /var/www/public;
{{- else }}
    root /var/www/html;
{{- end }}
    index index.html index.php;

    ############################################################################

    location = /healthz {
        if ($isHealthProbe != 1) {
          return 404;
        }
        add_header Content-Type text/plain;
        return 200 'IMOK';
    }

    ############################################################################
{{- if eq .Values.global.framework.name "drupal" }}

    location ~ ^/sites/.+/files/.+$ {
      try_files $uri /index.php?q=$uri;
    }

    ############################################################################

    location ~ /vendor/.*\.php$ {
      return 404;
    }

    ############################################################################
{{- end }}

    location ~ \.php {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;

        if (!-f $document_root$fastcgi_script_name) {
          return 404;
        }

        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_intercept_errors on;
        fastcgi_connect_timeout {{ .Values.web.nginx.fastcgi.connectTimeout }};
        fastcgi_send_timeout {{ .Values.web.nginx.fastcgi.sendTimeout }};
        fastcgi_read_timeout {{ add (max .Values.web.php.config.fpm.requestTerminateTimeoutSeconds (add (max .Values.web.php.config.core.maxExecutionTimeSeconds 1) (max .Values.web.php.config.core.maxInputTimeSeconds 1) 10)) 10 }};
        fastcgi_buffer_size 16k;
        fastcgi_buffers 16 16k;
        fastcgi_busy_buffers_size 32k;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME $fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    ############################################################################
{{- if (or (eq .Values.global.framework.name "drupal")
           (eq .Values.global.framework.name "laravel")
           (eq .Values.global.framework.name "symfony")) }}

    location / {
      try_files $uri /index.php$is_args$args;
    }

{{- end }}

}

################################################################################

